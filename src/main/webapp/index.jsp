<%-- 
    Document   : index
    Created on : 22-10-2020, 23:06:25
    Author     : estay
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <br>

        <div align="center"  ><h1><i>Evaluacion 3</i> </h1> </div>
            <br>
            <table class="table table-striped table-dark">
                <thead>
                    <tr>

                        <th scope="col">API</th>
                        <th scope="col">URL</th>
                        <th scope="col">USO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">GET</th>
                        <td>http://LAPTOP-LDRL10SD:8080/ventaautos-1/api/ventaautos </td>
                        <td>MEDIANTE EL SERVICIO REST, SE CONSTRUYE ESTE METODO, QUE  SE UTLIZA  PARA   CONSULTAR
                            LEER OBTENER DATOS, PARA ELLO SE UTILIZA LA APLICACION POSTMAN QUE  PERMITE UTILIZAR
                            DICHO SERVICIO PARA RETORNAR UNA LISTA DE LOS AUTOS REGISTRADOS O UN DATO ESPECIFICO,
                            CORROBORANDO EN LA BD DE POSTGRES LA  CREACION EN ESTE CASO DEL VEHICULO. </td> 

                    </tr>
                    <tr>
                        <th scope="row">POST</th>
                        <td>http://LAPTOP-LDRL10SD:8080/ventaautos-1/api/ventaautos</td>
                        <td>ESTE METODO SE UTILIZA PARA CREAR UN DATO, EL RECURSO ES EL MISMO QUE EL GET, PERO EL 
                            METODO ES DIFERENTE, UTILIZANDO EL POSTMAN EN BODY INSERTAMOS EL DATO A CREAR Y  NOS
                            RETORNA LA QUE EL DATO FUE CREADO EXITOSAMENTE, CORROBORANDO EN LA BD DE  POSTGRES LA
                            CREACION EN ESTE CASO DEL VEHICULO</td>

                    </tr>
                    <tr>
                        <th scope="row">DELETE</th>
                        <td>http://LAPTOP-LDRL10SD:8080/ventaautos-1/api/ventaautos/</td>
                        <td>ESTE METODO ES UTILIZADO PARA ELIMINAR UN RECURSO, UTILIZANDO EL POSTMAN SE  INSERTA
                            EN EL URL EL CODIGO DEL RECURSO, OBTENIENDO COMO RETORNO LA ELIMINACION DEL DATO.</td>

                    </tr>

                    </tr>
                    <tr>
                        <th scope="row">PUT</th>
                        <td>http://LAPTOP-LDRL10SD:8080/ventaautos-1/api/ventaautos</td>
                        <td>ESTE METODO PARA LA EDICION DE UN DATO, UTILIZANDO LA HERRAMIENTA POSTMAN, SE INSERTA
                            EL REGISTRO A ACTUALIZAR, MODIFICANDO SUS DATOS A EXCEPCION DEL CODIGO, HACEMOS CLIC   EN
                            SEND Y SE ACTUALIZA EL REGISTRO, LO CUAL PUEDE SER CORROBORADO EN LA BD</td>

                    </tr>
                </tbody>
            </table>
            <div>

                <ul>
                    <h6>DATOS GENERALES</h6>    
                    <li><span class="glyphicon glyphicon-envelope"></span> AUTOR: Claudio Estay Diaz</li>            
                    <li><span class="glyphicon glyphicon-map-marker"></span> SECCION: 50</li>         
                    <li><span class="glyphicon glyphicon-map-marker"></span> URL BITBUCKET:  https://42908500ANTONIO@bitbucket.org/42908500ANTONIO/ventaautos.git </li>
                    <li><span class="glyphicon glyphicon-map-marker"></span> URL HEROKU:     https://ventaautos.herokuapp.com </li>
                </ul>  

            </div>

    </body>
</html>



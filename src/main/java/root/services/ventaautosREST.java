package root.services;

import cl.dao.VentaautosJpaController;
import cl.dao.exceptions.NonexistentEntityException;
import cl.entities.Ventaautos;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/ventaautos")
public class ventaautosREST {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    EntityManager em;

    @GET //listar todo
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {

        VentaautosJpaController dao = new VentaautosJpaController();
        List<Ventaautos> lista = dao.findVentaautosEntities();
        System.out.println("lista.size():" + lista.size());
        return Response.ok(200).entity(lista).build();
    }

    @GET //buscar
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {
        VentaautosJpaController dao = new VentaautosJpaController();
        Ventaautos sel = dao.findVentaautos(idbuscar);
        return Response.ok(200).entity(sel).build();
    }

    @POST //crear
    @Produces(MediaType.APPLICATION_JSON)
    public Response nuevo(Ventaautos Ventaautos) {
        System.out.println("nuevo seleccion.getCodigo()" + Ventaautos.getCodigo());
        System.out.println("nuevo seleccion.getMarca()" + Ventaautos.getMarca());
        System.out.println("nuevo seleccion.getModelo()" + Ventaautos.getModelo());
        System.out.println("nuevo seleccion.getAño()" + Ventaautos.getAño());
        System.out.println("nuevo seleccion.getColor()" + Ventaautos.getColor());

       
        VentaautosJpaController dao = new VentaautosJpaController();
        try {
            dao.create(Ventaautos);
        } catch (Exception ex) {
            Logger.getLogger(ventaautosREST.class.getName()).log(Level.SEVERE, null, ex);
        }
       return Response.ok("vehiculo creado").build();
    }

    @PUT //actualizar
    public Response actualizar(Ventaautos Ventaautos) {
                VentaautosJpaController dao = new VentaautosJpaController();
        try {
            dao.edit(Ventaautos);
        } catch (Exception ex) {
            Logger.getLogger(ventaautosREST.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(Ventaautos).build();

    }

    @DELETE //borrar
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminaId(@PathParam("iddelete") String iddelete) {
        System.out.println("eliminaId :" + iddelete);
        VentaautosJpaController dao = new VentaautosJpaController();
        try {
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ventaautosREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("registro eliminado").build();

    }

}

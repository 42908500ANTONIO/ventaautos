/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dao;

import cl.dao.exceptions.NonexistentEntityException;
import cl.dao.exceptions.PreexistingEntityException;
import cl.entities.Ventaautos;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author estay
 */
public class VentaautosJpaController implements Serializable {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public VentaautosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public VentaautosJpaController() {

    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ventaautos ventaautos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ventaautos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findVentaautos(ventaautos.getCodigo()) != null) {
                throw new PreexistingEntityException("Ventaautos " + ventaautos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ventaautos ventaautos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ventaautos = em.merge(ventaautos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = ventaautos.getCodigo();
                if (findVentaautos(id) == null) {
                    throw new NonexistentEntityException("The ventaautos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ventaautos ventaautos;
            try {
                ventaautos = em.getReference(Ventaautos.class, id);
                ventaautos.getCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ventaautos with id " + id + " no longer exists.", enfe);
            }
            em.remove(ventaautos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ventaautos> findVentaautosEntities() {
        return findVentaautosEntities(true, -1, -1);
    }

    public List<Ventaautos> findVentaautosEntities(int maxResults, int firstResult) {
        return findVentaautosEntities(false, maxResults, firstResult);
    }

    private List<Ventaautos> findVentaautosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ventaautos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ventaautos findVentaautos(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ventaautos.class, id);
        } finally {
            em.close();
        }
    }

    public int getVentaautosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ventaautos> rt = cq.from(Ventaautos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}

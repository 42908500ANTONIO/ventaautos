/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author estay
 */
@Entity
@Table(name = "ventaautos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ventaautos.findAll", query = "SELECT v FROM Ventaautos v"),
    @NamedQuery(name = "Ventaautos.findByCodigo", query = "SELECT v FROM Ventaautos v WHERE v.codigo = :codigo"),
    @NamedQuery(name = "Ventaautos.findByMarca", query = "SELECT v FROM Ventaautos v WHERE v.marca = :marca"),
    @NamedQuery(name = "Ventaautos.findByModelo", query = "SELECT v FROM Ventaautos v WHERE v.modelo = :modelo"),
    @NamedQuery(name = "Ventaautos.findByA\u00f1o", query = "SELECT v FROM Ventaautos v WHERE v.año = :año"),
    @NamedQuery(name = "Ventaautos.findByColor", query = "SELECT v FROM Ventaautos v WHERE v.color = :color")})
public class Ventaautos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 2147483647)
    @Column(name = "marca")
    private String marca;
    @Size(max = 2147483647)
    @Column(name = "modelo")
    private String modelo;
    @Size(max = 2147483647)
    @Column(name = "año")
    private String año;
    @Size(max = 2147483647)
    @Column(name = "color")
    private String color;

    public Ventaautos() {
    }

    public Ventaautos(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ventaautos)) {
            return false;
        }
        Ventaautos other = (Ventaautos) object;
        return !((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo)));
    }

    @Override
    public String toString() {
        return "cl.entities.Ventaautos[ codigo=" + codigo + " ]";
    }
    
}
